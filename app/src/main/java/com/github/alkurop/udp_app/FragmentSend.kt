package com.github.alkurop.udp_app

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_send.*

class FragmentSend : Fragment(R.layout.fragment_send) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        send.setOnClickListener {
            sendUdp(
                UdpMessage(
                    port = Integer.parseInt(port.text.toString()),
                    message = message.text.toString().trim()
                )
            ).subscribe()
        }
    }
}