package com.github.alkurop.udp_app

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_receive.*


class FragmentReceive : Fragment(R.layout.fragment_receive) {
    private var listeningDisposable: Disposable? = null
    private val adapter = ReceiveAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        stop.setOnClickListener { stopListening() }
        listen.setOnClickListener { startListening() }
        received.layoutManager = LinearLayoutManager(requireContext())
        received.adapter = adapter
    }

    override fun onDestroyView() {
        stopListening()
        super.onDestroyView()
    }

    private fun startListening() {
        listen.isVisible = false
        stop.isVisible = true
        val portNumber = Integer.parseInt(port.text.toString())
        listeningDisposable =
            listenUdp(UdpListeningConfig(portNumber))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    adapter.addItem(it)
                }
    }

    private fun stopListening() {
        listeningDisposable?.dispose()
        listen.isVisible = true
        stop.isVisible = false
    }
}



