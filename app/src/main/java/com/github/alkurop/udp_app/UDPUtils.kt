package com.github.alkurop.udp_app

import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress


class UdpListeningConfig(val port: Int)

fun listenUdp(config: UdpListeningConfig): Observable<String> =
    Observable
        .create<String> { emitter ->
            val serverSocket = DatagramSocket(config.port)
            val receiveData = ByteArray(1024)
            while (!emitter.isDisposed) {
                val receivePacket =
                    DatagramPacket(receiveData, receiveData.size)
                serverSocket.receive(receivePacket)
                val text = String(receivePacket.data)
                emitter.onNext(text)
            }
            emitter.onComplete()
        }
        .subscribeOn(Schedulers.io())


class UdpMessage(val port: Int, val message: String)

fun sendUdp(message: UdpMessage): Completable =
    Completable
        .fromAction {
            val socket = DatagramSocket()
            val bytes = message.message.toByteArray()
            val ip = InetAddress.getLocalHost()
            val packet = DatagramPacket(bytes, bytes.size, ip, message.port)
            socket.send(packet);
        }
        .subscribeOn(Schedulers.io())
